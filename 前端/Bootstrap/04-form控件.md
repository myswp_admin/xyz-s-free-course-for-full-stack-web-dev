# form（表单）控件

## 1. 前端验证

### 1.1 概念

在浏览器中，直接验证用户的输入信息是否符合要求，并给出提示

### 1.2 原理

* HTML 表单验证通过 CSS 的两个伪类:invalid和:valid. 它适用于<input>，<select>，单选复选框 和<textarea>元素。
* Bootstrap 将:invalid和:valid样式范围限定为父.was-validated类，通常应用于<form>. 否则，任何没有值的必填字段在页面加载时都会显示为无效。这样，您可以选择何时激活它们（通常在尝试提交表单之后）。
* 要重置表单的外观（例如，在使用 AJAX 的动态表单提交的情况下），请在提交后删除.was-validated。
* 作为备用，.is-invalid并且.is-valid类别可以用来代替伪类用于服务器端验证。他们不需要.was-validated父类。
* 由于 CSS 工作方式的限制，我们（目前）无法在<label>没有自定义 JavaScript 帮助的情况下将样式应用于DOM 中表单控件之前的 a 。
* 所有现代浏览器都支持约束验证 API，这是一系列用于验证表单控件的 JavaScript 方法。
* 反馈消息可能会使用浏览器默认值（每个浏览器不同，并且无法通过 CSS 设置样式）或我们的自定义反馈样式以及附加的 HTML 和 CSS。
您可以setCustomValidity在 JavaScript 中提供自定义有效性消息。

### 1.3 使用步骤

1. 挂接 bootstrap 5.10 ,css 和 js 库都需要
2. 所有需要验证的控件，需要包入 form 表单中
3. form 表单标签上需要加上 .needs-validation 类，初始时需要加上 novalidate 属性
4. 需要验证的`<input>`，`<select>`和`<textarea>` 等标签，需要进行验证的，加上 required 属性；
5. 需要验证的标签，如果需要自定义的验证反馈信息，需要紧跟一个样式为 .invalid-feedback（未通过验证的信息） 或 .valid-feedback （通过验证的信息） 的层，内部写上自定义的反馈信息，当然，也可以两个都有
6. 在客户端脚本中，写入验证相关逻辑，具体信息参考例子代码
7. input-group 需要挂接一个额外的.has-validation类。

### 1.4 服务器端验证

有些信息需要在服务器端进行验证，这些需要放行，使用.is-invalid 和 .is-valid类别可以用来代替伪类用于服务器端验证。他们不需要.was-validated父类。

逻辑：
* 需要服务器端验证的，可以先将类挂为 .is-valid （通过验证），将该 form 控件对应信息提交给服务器；
* 服务器通过验证，则保持原样；服务器未通过验证，则通过 js 再将其样式挂为 .is-invalid (未通过验证)

> 建议：
> * 任何能在客户端进行验证的，最好在客户端直接验证
> * 一些必须在服务器端进行验证的，则通过 ajax 将数据提交服务器验证，如用户名是否重复
> * 在一些安全性要求较高的应用中，可以同时设置客户端和服务器端双重验证

### 1.5 使用 tooltip (悬停标签) 来显示反馈信息

使用 .valid-tooltip / .invalid-tooltip 样式，替换样式 .valid-feedback / .invalid-feedback， 可以将普通的反馈信息，替换为悬停标签（tooltip）样式的 反馈信息

> 参考资料： 
> [MDN 自定义验证](https://developer.mozilla.org/en-US/docs/Learn/Forms/Form_validation)

> 第三方验证库：  
> [https://formvalidation.io/](https://formvalidation.io/)