
const path = require('path')
const webpack= require('webpack')
module.exports = {
    entry: {
        index:['./src/index','webpack-hot-middleware/client'],
    },
    mode: 'development',
    devtool:'inline-source-map',
    devServer: {
        static:'./',
        hot:true,
        port: 9000,
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
};

/*
output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.js',
        publicPath:'/',
    },
    */