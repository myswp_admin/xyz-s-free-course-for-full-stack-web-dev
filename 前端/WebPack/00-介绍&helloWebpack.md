# 总介

## 1. 什么是 Webpack 

前端项目打包工具，能够合理科学对前端项目进行模块化管理，将一切按需求打包在一起。

使用 webpack 的优点：

* 打包一切，让前端项目被浏览器加载更快速
* 模块化开发，让前端项目结构变得简单、有条理
* 优化前端项目结构，能够系统构建大型项目，井井有条；

给出官方概念：  
webpack 是一个现代 JavaScript 应用程序的静态模块打包器(module bundler)。当 webpack 处理应用程序时，它会递归地构建一个依赖关系图(dependency graph)，其中包含应用程序需要的每个模块，然后将所有这些模块打包成一个或多个 bundle。

![](../../imgs/webpack_00_01_原理.png)

## 2. 环境配置

1. 安装 nodejs （同时会装上npm）：[官网地址](https://nodejs.org/en/download/)
2. 更新 npm 并配置本地源
``` shell
# 换淘宝源，增加 js 库下载速度
npm config set registry https://registry.npm.taobao.org
# 更新 npm
npm install -g npm
# 安装 yarn (可选)，yarn 可以替换 npm 来使用，更加快速方便
npm install -g yarn
```

## 3. Demo-01 hello webpack

代码位置：[demo-01](https://gitee.com/chutianshu1981/xyz-s-free-course-for-full-stack-web-dev/tree/main/%E5%89%8D%E7%AB%AF/WebPack/codes/demo_01_hello_webpack)

步骤：

1. 新建项目文件夹 demo_01_hello_webpack （你自己的可以简单些），在终端中，进入项目根目录，执行命令：npm init -y，初始化项目（实际上就是增加了一个 package.json 文件，用来配合 npm 管理项目）
2. 根目录新建文件 index.html，在其中加入：
```html
<!--加载webpack打包后的js-->
<script src="./dist/main.js"></script>
```
3. 新建子文件夹 src （这个不要改），并在其中新建文件 index.js，代码如下：
``` js
// ./src/index.js
document.write('<div style="color:red;font-size:18px">Hello WebPack</div>');
```
4. 在终端中，进入项目根目录，执行命令：npm install --save-dev webpack webpack-cli ，观察 package.json 的变化，增加了下面一项，项目在开发状态下增加的依赖
``` json
"devDependencies": {
    "webpack": "^5.51.1",
    "webpack-cli": "^4.8.0"
  }
```
5. 运行 webpack ,打包项目。在项目根目录下打开终端，运行下面命令，对项目打包
``` shell
npx webpack
```
会发现，在根目录下，多了 ./dist/main.js ，这个就是对项目中 js 脚本（./src/index.js）按照 webpack 默认方式打包的结果

> npx 现阶段可以简单理解为 npm 自带的升级版命令执行器，主要作用是，在执行 npm 命令时，可以自动查找，并提示安装项目中缺失的依赖库  
> [阮一峰 npx 使用教程](https://www.ruanyifeng.com/blog/2019/02/npx.html)

6. 资源管理器中双击 index.html ，查看结果 

## 4. 示例解析

上面例子演示了最简单、最基本的 webpack 打包功能，没有对 webpack 进行任何设置，全都用的是 webpack 的默认配置，将 ./src/index.js 打包，结果输出为 ./dist/main.js

我们可以得出结论：  
* webpack 打包默认入口（entry）： ./src/index.js
* webpack 打包默认输出（output）：./dist/main.js

## 5. 概念讲解

### 5.1 入口 entry

白话就是项目打包的起始 js 

默认值是 ./src/index.js，但你可以通过在 webpack configuration 中配置 entry 属性，来指定一个（或多个）不同的入口起点。

[入口 官方文档中文版](https://webpack.docschina.org/concepts/#entry)

### 5.2 输出 output

webpack 打包后的结果

主要输出文件的默认值是 ./dist/main.js，其他生成文件默认放置在 ./dist 文件夹中。

[输出 官方文档中文版](https://webpack.docschina.org/concepts/#output)

## 6. 学习资源

* [官方站 webpack.js.org](https://webpack.js.org/)
* [中文站 webpackjs.com](https://www.webpackjs.com/)
* [中文站 webpack.docschina.org](https://webpack.docschina.org/)
* [阮一峰 webpack 教程](https://github.com/ruanyf/webpack-demos)


> 注意：  
> [lodash库文档](https://www.lodashjs.com/)
